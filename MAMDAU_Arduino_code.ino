/*  This code was developed by the Kaiserslautern High School National Technical Honor Society
 *   for use on the Moore Airborne Modular Data Acquisition Unit(MAMDAU). For usage and 
 *   instructions refer to documentation in the extracted folder.
*/
#include <SoftwareSerial.h>
#include <SparkFun_VEML6075_Arduino_Library.h>

// tx and rx pins for the ozone sensor 
SoftwareSerial OzoneSerial(11, 12);          


// ints for the temperature sensors
int sensorPin1 = 0;                        
int sensorPin2 = 1;
int sensorPin3 = 2;

// create class for uv sensor
VEML6075 uv; 

// setup code
void setup()
{
  // initiate the serial connection to the pi
  Serial.begin(9600);

  // start the connection to the uv sensor  
  if (!uv.begin())
  {
    Serial.println("VEML6075 not detected!");
    while (1) ;
  }  
  OzoneSerial.begin(9600);   
  String start = "UVA,UVB,UV Index,Uncovered Temp,Celaphane Temp,Tin Foil Temp\n";           
}

// main loop
void loop()                     
{
  
 //getting the voltage reading from the temperature sensor
 int reading1 = analogRead(sensorPin1);  
 int reading2 = analogRead(sensorPin2); 
 int reading3 = analogRead(sensorPin3);
 
 // converting reading to voltage
 double voltage1 = reading1 * 5.0;
 double voltage2 = reading2 * 5.0;
 double voltage3 = reading3 * 5.0;
 voltage1 /= 1024.0; 
 voltage2 /= 1024.0;
 voltage3 /= 1024.0;
  
 // now print out the temperature
 //converting from 10 mv per degree wit 500 mV offset to degrees ((voltage - 500mV) times 100
 double temperatureC1 = (voltage1 - 0.5) * 100 ;  
 double temperatureC2 = (voltage2 - 0.5) * 100 ;
 double temperatureC3 = (voltage3 - 0.5) * 100 ;
 
 // output strings                                          
 String uvaout = String(uv.uva()) + ",";
 String uvbout = String(uv.uvb()) + ",";
 String uviout = String(uv.index()) +  ",";
 String out1 = String(temperatureC1) + ",";
 String out2 = String(temperatureC2) + ",";
 String out3 = String(temperatureC3) + "\n";
 String data = uvaout + uvbout + uviout + out1 + out2 + out3;
 
 // send the data to the pi
 Serial.print(data);
 
 // this is the delay between data transmissions
 delay(1000);                                     
}

